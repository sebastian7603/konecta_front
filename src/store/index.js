import Vue from 'vue';
import Vuex from 'vuex';
import axios from '@/axios'

Vue.use(Vuex);

export default new Vuex.Store({

    state: {
        currentUser: [],
        statusLogin: false,
        roles: [],
        users: [],
        customers: [],
        docTypes: []
    },
    getters: {
        getCurrentUser(state){
            return state.currentUser
        },
        getStatusLogin(state){
            return state.statusLogin
        },
        getRoles(state){
            return state.roles
        },
        getUsers(state){
            return state.users
        },
        getCustomers(state){
            return state.customers
        },
        getDocTypes(state){
            return state.docTypes
        }
    },
    actions: {
        showErrors(context, err){
            console.log(err.config);
            if (err.response) {
                console.log(err.response.data);
                console.log(err.response.status);
                console.log(err.response.headers);
            } else if (err.request) {
                console.log(err.request);
            } else {
                console.log('Error', err.message);
            }
        },
        allRoles(context){
            axios.get('roles')
                .then((res) => {
                    context.commit('roles', res.data.roles)
                }, 200)
                .catch((err) => {
                    this.dispatch('showErrors', err)
                })
        },
        allUsers(context){
            axios.get('users', {
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.token,
                        'role_id': localStorage.role_id
                    }
                })
                .then((res) => {
                    context.commit('users', res.data.users)
                }, 200)
                .catch((err) => {
                    this.dispatch('showErrors', err)
                })
        },
        allCustomers(context){
            axios.get('customers', {
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.token,
                        'role_id': localStorage.role_id
                    }
                })
                .then((res) => {
                    console.log(res);
                    context.commit('customers', res.data.customers)
                }, 200)
                .catch((err) => {
                    this.dispatch('showErrors', err)
                })
        },
        allDocTypes(context){
            axios.get('doc-types', {
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.token,
                        'role_id': localStorage.role_id
                    }
                })
                .then((res) => {
                    context.commit('docTypes', res.data.docTypes)
                }, 200)
                .catch((err) => {
                    this.dispatch('showErrors', err)
                })
        },
        currentUser(context){
            axios.post('auth/me', {}, {
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.token,
                        'role_id': localStorage.role_id
                    }
                })
                .then((res) => {
                    context.commit('currentUser', res.data.user)
                    context.commit('statusLogin', true)
                })
                .catch((err) => {
                    localStorage.token = ''
                    localStorage.role_id = ''
                    context.commit('currentUser', null)
                    context.commit('statusLogin', false)
                    this.dispatch('showErrors', err)
                    window.location = '/#/login'
                })
        }
    },
    mutations: {
        currentUser(state, data){
            return state.currentUser = data
        },
        statusLogin(state, data){
            return state.statusLogin = data
        },
        roles(state, data) {
            return state.roles = data
        },
        users(state, data) {
            return state.users = data
        },
        customers(state, data) {
            return state.customers = data
        },
        docTypes(state, data) {
            return state.docTypes = data
        }
    }
});