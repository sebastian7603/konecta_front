import Vue from 'vue'
import App from './App.vue'
import Router from 'vue-router'
import store from './store'
import axios from './axios'
import vuetify from './plugins/vuetify'
import Vuex from 'vuex'

// Views
import Admin from './views/Admin'
import Login from './views/Login'
import Register from './views/Register'
import Customers from './views/Customers'
import CreateCustomer from './views/CreateCustomer'
import Users from './views/Users'

Vue.use(Router);

let router = new Router({
    routes: [
        {
            name: 'login',
            path: '/login',
            component: Login,
        },
        {
            path: '/customers',
			component: Admin,
            children: [
                {
                    name: 'customers',
                    path: '',
                    component: Customers,
                },
                {
                    name: 'customers.new',
                    path: 'new',
                    component: CreateCustomer,
                },
            ]
		},
        {
            path: '/users',
			component: Admin,
            children: [
                {
                    name: 'users',
                    path: '',
                    component: Users,
                },
                {
                    name: 'users.new',
                    path: 'new',
                    component: Register,
                },
            ]
		}
    ]
});

Vue.config.productionTip = false;
Vue.prototype.$http = axios; 
axios.defaults.timeout = 10000;
axios.interceptors.request.use(
    config => {
        const token = 'Bearer ' + localStorage.access_token
        if (token) {
            config.headers.common["Authorization"] = token
        }
        return config;
    },
    error => {
        return Promise.reject(error);
    }
);
axios.interceptors.response.use(
    response => {
        if (response.status === 200 || response.status === 201) {
            return Promise.resolve(response);
        } else {
            return Promise.reject(response);
        }
    },
    error => {
        if (error.response.status) {
            return Promise.reject(error.response);
        }
    }
);

new Vue({
    el: '#app',
    axios,
	router,
	store,
	vuetify,
	Vuex,
	render: h => h(App)
}).$mount('#app')
