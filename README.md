# konecta_front
Link demostrativo [https://youtu.be/6KbuNglWlIs](https://youtu.be/6KbuNglWlIs).


## Requisitos para deployar
- [Node Js](https://nodejs.org/).
- [Git](https://git-scm.com/).
- (Opcional) Empaquetado de desarrollo. Recomiendo [XAMPP](https://www.apachefriends.org/es/index.html).

## Repositorio del API
[https://bitbucket.org/sebastian7603/konecta/src/master/](https://bitbucket.org/sebastian7603/konecta/src/master/).

## Pasos para deployar

### Ubicarse en la carpeta deseada y clonar el repositorio
```
git clone https://bitbucket.org/sebastian7603/konecta_front.git
```

### Ejecutar la instalación de paquetes
```
cd konecta_front
npm install
```

### Ejecutar el servidor de vue (*)
Se debe configurar en el .env la ruta del api
```
npm run serve
```

### Generar archivos de despliegue en apache o nginx
Se debe configurar en el .env la ruta del api
```
npm run build
```
